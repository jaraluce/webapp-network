package me.relevante.web.model.card;

import me.relevante.model.NetworkSearchUser;
import me.relevante.model.RelevanteAccount;
import me.relevante.model.RelevanteContext;
import me.relevante.network.Network;
import me.relevante.utils.DateFormatter;
import me.relevante.web.model.json.card.NetworkCardUser;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractNetworkSearchCardMapper<N extends Network, S extends NetworkSearchUser<N, ?, ?>, C extends NetworkCardUser<N>>
        implements NetworkSearchCardMapper<N, S, C> {

	@Autowired protected DateFormatter dateFormatter;
	@Autowired protected CardActionProgressOutput networkActionProgressOutput;

    @Override
    public abstract C mapSearchUser(S searchUser,
                                    RelevanteAccount relevanteAccount,
                                    RelevanteContext relevanteContext);

    protected List<String> mapWatchlists(List<String> watchlistNames) {
        List<String> mappedWatchlists = new ArrayList<>();
        watchlistNames.forEach(watchlistName -> mappedWatchlists.add(watchlistName));
        return mappedWatchlists;
    }

    protected List<String> mapTags(List<String> tagNames) {
        List<String> mappedTags = new ArrayList<>();
        tagNames.forEach(tagName -> mappedTags.add(tagName));
        return mappedTags;
    }

    protected List<String> getCardRelatedTerms(List<String> relatedTerms) {
        List<String> cardRelatedTerms = new ArrayList<>();
        relatedTerms.forEach(relatedTerm -> cardRelatedTerms.add(relatedTerm.replace("+", " ")));
        return cardRelatedTerms;
    }

}
