package me.relevante.web.model.card;

import me.relevante.model.NetworkEntity;
import me.relevante.model.NetworkFullUser;
import me.relevante.model.RelevanteAccount;
import me.relevante.model.RelevanteContext;
import me.relevante.network.Network;
import me.relevante.web.model.json.card.NetworkCardUser;

public interface NetworkFullCardMapper<N extends Network, F extends NetworkFullUser<N, ?>, U extends NetworkCardUser<N>> extends NetworkEntity<N> {
    U mapFullUser(F fullUser, RelevanteAccount relevanteAccount, RelevanteContext relevanteContext);
}
