package me.relevante.web.model.oauth;

import me.relevante.api.OAuthKeyPair;
import me.relevante.api.OAuthTokenPair;
import me.relevante.model.NetworkEntity;
import me.relevante.network.Network;

public interface NetworkOAuthManager<N extends Network> extends NetworkEntity<N> {

    OAuthAccessRequest<N> createOAuthRequest(String callbackUrl);
    OAuthTokenPair<N> obtainAccessToken(String requestTokenString,
                                        String requestSecret,
                                        String requestVerifierString);
    OAuthKeyPair<N> getOAuthConsumerKeyPair();
}
