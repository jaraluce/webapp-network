package me.relevante.web.model.search;

import me.relevante.model.NetworkEntity;
import me.relevante.model.NetworkSearchUser;
import me.relevante.network.Network;

import java.util.List;

/**
 * Created by daniel-ibanez on 16/07/16.
 */
public interface NetworkSearcher<N extends Network, S extends NetworkSearchUser<N, ?, ?>> extends NetworkEntity<N> {
    List<S> execute(List<String> searchTerms);
}
