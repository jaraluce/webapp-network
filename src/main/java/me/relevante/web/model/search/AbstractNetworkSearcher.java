package me.relevante.web.model.search;

import me.relevante.model.NetworkFullUser;
import me.relevante.model.NetworkSearchUser;
import me.relevante.model.NetworkSignal;
import me.relevante.network.Network;
import me.relevante.nlp.NetworkStemUserIndex;
import me.relevante.nlp.core.NlpCore;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public abstract class AbstractNetworkSearcher<N extends Network, F extends NetworkFullUser<N, ?>, S extends NetworkSearchUser<N, F, ?>, I extends NetworkStemUserIndex<N>>
        implements NetworkSearcher<N, S> {

    private static final int MAX_POSTS = 12;
    private static final int MAX_USERS = 1000;

    protected NlpCore nlpCore;

    public AbstractNetworkSearcher(NlpCore nlpCore) {
        this.nlpCore = nlpCore;
    }

    @Override
    public List<S> execute(List<String> searchTerms) {
        List<SearchQuery> multipleQuery = getMultipleQuery(searchTerms, 2);
        List<String> userIds = search(multipleQuery);
        List<S> searchUsers = findSearchUsersById(userIds);
        Map<String, S> searchUsersById = createMapFromSearchUsersList(searchUsers);
        List<S> sortedSearchUsers = createListInSearchOrder(searchUsersById, userIds);
        removeNonRelatedSignals(searchUsersById, multipleQuery);
        removeUsersWithoutSignals(userIds, sortedSearchUsers);
        List<F> fullUsers = findFullUsersById(userIds);
        completeDataFromFullUsers(searchUsersById, fullUsers);
        assignPostsToUsers(sortedSearchUsers, MAX_POSTS);
        return sortedSearchUsers;
    }

    private List<SearchQuery> getMultipleQuery(List<String> terms,
                                               int maxTerms) {

        List<SearchQuery> multipleQuery = new ArrayList<>();
        for (String term : terms) {
            String lowercaseTerm = term.toLowerCase();
            List<String> termWords = nlpCore.removeStopWords(lowercaseTerm);
            List<String> cleanTerms = new ArrayList<>();
            termWords.forEach(termWord -> cleanTerms.add(nlpCore.stem(termWord)));
            SearchQuery query = new SearchQuery(cleanTerms, maxTerms);
            multipleQuery.add(query);
        }
        return multipleQuery;
    }

    private List<String> search(List<SearchQuery> multipleQuery) {
        // Create map containing each user with his scores (one by each terms)
        Map<String, List<Double>> scoresByUser = new HashMap<>();
        for (SearchQuery searchQuery : multipleQuery) {
            List<I> idxs = findStemUserIndexByStem(searchQuery.getMainQueryTerm(), MAX_USERS);
            for (I idx : idxs) {
                List<Double> userScores = scoresByUser.get(idx.getUserId());
                if (userScores == null) {
                    userScores = new ArrayList<>();
                    scoresByUser.put(idx.getUserId(), userScores);
                }
                userScores.add(idx.getScore());
            }
        }
        // Sort scores in descending order
        LinkedHashMap<String, Double> combinedScoreByUser = new LinkedHashMap<>();
        for (Map.Entry<String, List<Double>> entry : scoresByUser.entrySet()) {
            List<Double> scores = entry.getValue();
            scores.sort((o1, o2) -> o2.compareTo(o1));
            int i = multipleQuery.size();
            Double combinedScore = 0.0;
            for (Double score : scores) {
                combinedScore = combinedScore + score * Math.pow(100, i);
                i--;
            }
            combinedScoreByUser.put(entry.getKey(), combinedScore);
        }
        List<Map.Entry<String, Double>> entries = new ArrayList<>(combinedScoreByUser.entrySet());
        entries.sort((o1, o2) -> o2.getValue().compareTo(o1.getValue()));
        List<String> userIds = new ArrayList<>();
        entries.forEach(entry -> userIds.add(entry.getKey()));

        return userIds;
    }

    protected abstract List<I> findStemUserIndexByStem(String stem, int maxUsers);

    protected abstract List<S> findSearchUsersById(List<String> userIds);

    protected abstract List<F> findFullUsersById(List<String> userIds);

    private Map<String, S> createMapFromSearchUsersList(List<S> searchUsers) {
        int initialCapacity = (int) (searchUsers.size() / 0.75) + 1;
        Map<String, S> searchUsersById = new HashMap<>(initialCapacity);
        searchUsers.forEach(searchUser -> searchUsersById.put(searchUser.getId(), searchUser));
        return searchUsersById;
    }

    private List<S> createListInSearchOrder(Map<String, S> searchUsersById,
                                                             List<String> userIds) {
        // Create list preserving the original userIds order
        List sortedSearchUsers = new ArrayList<>();
        for (String userId : userIds) {
            sortedSearchUsers.add(searchUsersById.get(userId));
        }
        return sortedSearchUsers;
    }

    private void removeUsersWithoutSignals(List<String> userIds,
                                             List<S> searchUsers) {
        List<S> usersWithSignals = new ArrayList<>();
        List<String> userIdsWithSignals = new ArrayList<>();
        for (S searchUser : searchUsers) {
            if (!searchUser.getSignals().isEmpty()) {
                userIdsWithSignals.add(searchUser.getId());
                usersWithSignals.add(searchUser);
            }
        }
        userIds.clear();
        userIds.addAll(userIdsWithSignals);
        searchUsers.clear();
        searchUsers.addAll(usersWithSignals);
    }

    private void removeNonRelatedSignals(Map<String, S> searchUsersById,
                                           List<SearchQuery> queries) {
        for (S searchUser : searchUsersById.values()) {
            List<NetworkSignal<N>> relatedSignals = new ArrayList<>();
            for (NetworkSignal<N> signal : searchUser.getSignals()) {
                for (SearchQuery query : queries) {
                    if (isRelatedSignal(signal, query)) {
                        relatedSignals.add(signal);
                        break;
                    }
                }
            }
            searchUser.getSignals().clear();
            searchUser.getSignals().addAll(relatedSignals);
        }
    }

    private void completeDataFromFullUsers(Map<String, S> searchUsersById,
                                             List<F> fullUsers) {
        for (F fullUser : fullUsers) {
            S searchUser = searchUsersById.get(fullUser.getId());
            searchUser.setFullUser(fullUser);
        }
    }

    private boolean isRelatedSignal(NetworkSignal<N> signal,
                                    SearchQuery query) {
        List<String> stems = signal.getContentStems();
        if (!stems.contains(query.getMainQueryTerm()))
            return false;
        for (String relatedStem : query.getLeftoverQueryTerms()) {
            if (!stems.contains(relatedStem)) {
                return false;
            }
        }
        return true;
    }

    protected abstract void assignPostsToUsers(List<S> searchUsers, int maxPosts);

}
