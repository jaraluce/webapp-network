package me.relevante.web.model.validation;

import me.relevante.model.NetworkFullUser;
import me.relevante.model.NetworkSearchUser;
import me.relevante.network.Network;

public abstract class AbstractNetworkSearchUserValidator<N extends Network, S extends NetworkSearchUser<N, F, ?>, F extends NetworkFullUser<N, ?>>
        extends WrapperValidator<N, S, F> implements NetworkSearchUserValidator<N, S> {

    public AbstractNetworkSearchUserValidator(NetworkFullUserValidator<N, F> wrappedValidator) {
        super(wrappedValidator);
    }

    @Override
    protected F obtainWrappedObjectFromObject(S searchUser) {
        return searchUser.getFullUser();
    }
}
