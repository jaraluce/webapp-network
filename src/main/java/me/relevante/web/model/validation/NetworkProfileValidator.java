package me.relevante.web.model.validation;

import me.relevante.model.NetworkProfile;
import me.relevante.network.Network;

public interface NetworkProfileValidator<N extends Network, T extends NetworkProfile<N, ?>> extends NetworkValidator<N, T> {
}
