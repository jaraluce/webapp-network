package me.relevante.web.model.validation;

import me.relevante.model.NetworkFullUser;
import me.relevante.model.NetworkProfile;
import me.relevante.network.Network;

public abstract class AbstractNetworkFullUserValidator<N extends Network, T extends NetworkFullUser<N, P>, P extends NetworkProfile<N, ?>>
        extends WrapperValidator<N, T, P> implements NetworkFullUserValidator<N, T> {

    public AbstractNetworkFullUserValidator(NetworkProfileValidator<N, P> wrappedValidator) {
        super(wrappedValidator);
    }

    @Override
    protected P obtainWrappedObjectFromObject(T fullUser) {
        return fullUser.getProfile();
    }
}
