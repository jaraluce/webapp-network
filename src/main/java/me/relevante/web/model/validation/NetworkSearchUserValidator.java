package me.relevante.web.model.validation;

import me.relevante.model.NetworkSearchUser;
import me.relevante.network.Network;

public interface NetworkSearchUserValidator<N extends Network, T extends NetworkSearchUser<N, ?, ?>> extends NetworkValidator<N, T> {
}
