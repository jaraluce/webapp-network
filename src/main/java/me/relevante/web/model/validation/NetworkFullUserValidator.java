package me.relevante.web.model.validation;

import me.relevante.model.NetworkFullUser;
import me.relevante.network.Network;

public interface NetworkFullUserValidator<N extends Network, T extends NetworkFullUser<N, ?>> extends NetworkValidator<N, T> {
}
