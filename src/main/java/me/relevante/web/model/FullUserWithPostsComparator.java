package me.relevante.web.model;


import me.relevante.model.NetworkFullUser;
import me.relevante.model.NetworkPostsContainer;
import me.relevante.network.Network;
import org.springframework.stereotype.Component;

import java.util.Comparator;
import java.util.Date;

@Component
public class FullUserWithPostsComparator<N extends Network, F extends NetworkFullUser<N, ?> & NetworkPostsContainer<N, ?>> implements Comparator<F> {

	@Override
    public int compare(F o1, F o2) {
		if (o1.getLastPosts().size() == 0 && o2.getLastPosts().size() == 0) {
			return 0;
		} else if (o1.getLastPosts().size() == 0) {
			return 1;
		} else if (o2.getLastPosts().size() == 0) {
			return -1;
		}
		Date lastTimestamp1 = o1.getLastPosts().get(0).getPost().getCreationTimestamp();
        Date lastTimestamp2 = o2.getLastPosts().get(0).getPost().getCreationTimestamp();
        if (!lastTimestamp1.equals(lastTimestamp2)) {
            return lastTimestamp2.compareTo(lastTimestamp1);
        }
        String lastPost1 = o1.getLastPosts().get(0).getPost().getId();
        String lastPost2 = o2.getLastPosts().get(0).getPost().getId();
        if (!lastPost1.equals(lastPost2)) {
            return lastPost2.compareTo(lastPost1);
        }
        String author1 = (o1.getLastPosts().get(0).getPost().getAuthorId().equals(o1.getUserId())) ? "" : o1.getUserId();
        String author2 = (o1.getLastPosts().get(0).getPost().getAuthorId().equals(o1.getUserId())) ? "" : o2.getUserId();
		return author2.compareTo(author1);
	}
}
