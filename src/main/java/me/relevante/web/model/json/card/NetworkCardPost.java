package me.relevante.web.model.json.card;


import me.relevante.model.NetworkEntity;
import me.relevante.network.Network;

import java.io.Serializable;

public interface NetworkCardPost<N extends Network> extends NetworkEntity<N>, Serializable {
}
