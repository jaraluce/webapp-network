package me.relevante.web.model.json.card;

import me.relevante.network.Network;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractNetworkCardUser<N extends Network> implements NetworkCardUser<N> {

    protected String type;
    protected String id;
    protected boolean isContact;
    protected List<String> notes;
    protected List<String> watchlists;
    protected List<String> keywords;
    protected boolean tapDone;
    protected boolean touchDone;
    protected List<CardSignal> signals;

    public AbstractNetworkCardUser() {
        this.notes = new ArrayList<>();
        this.watchlists = new ArrayList<>();
        this.keywords = new ArrayList<>();
        this.signals = new ArrayList<>();
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean getIsContact() {
        return isContact;
    }

    public void setIsContact(boolean isContact) {
        this.isContact = isContact;
    }

    public List<String> getNotes() {
        return notes;
    }

    public List<String> getWatchlists() {
        return watchlists;
    }

    public List<String> getKeywords() {
        return keywords;
    }

    public List<CardSignal> getSignals() {
        return signals;
    }

    public boolean isTapDone() {
        return tapDone;
    }

    public void setTapDone(boolean tapDone) {
        this.tapDone = tapDone;
    }

    public boolean isTouchDone() {
        return touchDone;
    }

    public void setTouchDone(boolean touchDone) {
        this.touchDone = touchDone;
    }
}
