package me.relevante.web.model.json;

import me.relevante.network.Network;

/**
 * Created by daniel-ibanez on 22/09/16.
 */
public abstract class NetworkAbstractActionJson<N extends Network> implements NetworkActionJson<N> {

    protected String id;
    protected String network;
    protected String action;

    public NetworkAbstractActionJson(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public String getNetwork() {
        return network;
    }

    public String getAction() {
        return action;
    }
}
