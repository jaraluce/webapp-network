package me.relevante.web.model.json;

import me.relevante.model.NetworkEntity;
import me.relevante.model.NetworkProfile;
import me.relevante.network.Network;
import org.json.simple.JSONObject;

public interface NetworkLoggedUserJsonMapper<N extends Network, T extends NetworkProfile<N, ?>> extends NetworkEntity<N> {

    JSONObject mapNetworkProfileToJson(T networkProfile);
}