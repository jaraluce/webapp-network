package me.relevante.web.persistence;

import me.relevante.model.RelevanteAccount;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RelevanteAccountRepo extends MongoRepository<RelevanteAccount, String> {

    RelevanteAccount findOneByCredentialsNetworkAndCredentialsUserId(String network, String userId);
    RelevanteAccount save(RelevanteAccount relevanteAccount);

}