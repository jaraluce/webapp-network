package me.relevante.web.service;

import me.relevante.model.NetworkEntity;
import me.relevante.network.Network;

public interface NetworkActionService<N extends Network> extends NetworkEntity<N> {

    String putLevel1Action(String relevanteId,
                           String postId);

    String postLevel2Action(String relevanteId,
                            String postId,
                            String commentText);

    String putLevel3Action(String relevanteId,
                           String targetUserId);

    String postLevel4Action(String relevanteId,
                            String targetUserId,
                            String subject,
                            String message);

}
