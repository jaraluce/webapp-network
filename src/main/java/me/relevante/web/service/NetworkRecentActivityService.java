package me.relevante.web.service;

import me.relevante.model.NetworkEntity;
import me.relevante.model.NetworkFullUser;
import me.relevante.model.RelevanteAccount;
import me.relevante.model.RelevanteContext;
import me.relevante.network.Network;

import java.util.List;

/**
 * Created by daniel-ibanez on 17/07/16.
 */
public interface NetworkRecentActivityService<N extends Network, T extends NetworkFullUser> extends NetworkEntity<N> {

    List<T> getNetworkRecentlyUpdatedUsers(RelevanteAccount relevanteAccount,
                                           RelevanteContext relevanteContext);

}
