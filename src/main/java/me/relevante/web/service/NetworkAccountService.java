package me.relevante.web.service;

import me.relevante.model.NetworkEntity;
import me.relevante.model.NetworkFullUser;
import me.relevante.model.RelevanteAccount;
import me.relevante.network.Network;
import me.relevante.web.model.RegisterResult;

public interface NetworkAccountService<N extends Network, U extends NetworkFullUser<N, ?>> extends NetworkEntity<N> {

    U getNetworkUser(RelevanteAccount relevanteAccount);
    RegisterResult<U> registerOAuthNetworkAccount(String existingRelevanteId,
                                                  String oAuthRequestToken,
                                                  String oAuthRequestSecret,
                                                  String oAuthRequestOriginalToken,
                                                  String oAuthRequestVerifier);
    RegisterResult<U> registerBasicAuthNetworkAccount(String existingRelevanteId,
                                                      String url,
                                                      String username,
                                                      String password);
    void unregisterNetworkAccount(String relevanteId);

}

