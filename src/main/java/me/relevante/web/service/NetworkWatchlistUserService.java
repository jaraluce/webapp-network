package me.relevante.web.service;

import me.relevante.model.NetworkEntity;
import me.relevante.model.NetworkFullUser;
import me.relevante.model.RelevanteAccount;
import me.relevante.model.RelevanteContext;
import me.relevante.network.Network;

import java.util.List;

public interface NetworkWatchlistUserService<N extends Network, T extends NetworkFullUser<N, ?>> extends NetworkEntity {

    List<T> getNetworkWatchlistUsers(RelevanteAccount relevanteAccount,
                                     RelevanteContext relevanteContext,
                                     String watchlistId);
}
