package me.relevante.web.service;

import me.relevante.model.NetworkEntity;
import me.relevante.model.NetworkFullUser;
import me.relevante.model.RelevanteAccount;
import me.relevante.model.RelevanteContext;
import me.relevante.network.Network;

import java.util.List;

public interface NetworkBlacklistUserService<N extends Network, T extends NetworkFullUser> extends NetworkEntity<N> {

    List<T> getNetworkBlacklistUsers(RelevanteAccount relevanteAccount,
                                     RelevanteContext relevanteContext);
}
