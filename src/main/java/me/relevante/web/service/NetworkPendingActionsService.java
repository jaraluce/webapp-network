package me.relevante.web.service;

import me.relevante.model.NetworkEntity;
import me.relevante.network.Network;
import me.relevante.web.model.json.NetworkActionJson;

import java.util.List;

public interface NetworkPendingActionsService<N extends Network> extends NetworkEntity<N> {

    List<NetworkActionJson<N>> getPendingActions(String authorId);
    void acknowledgeActionDone(String actionId, String actionName);
    void acknowledgeActionError(String actionId, String actionName);
}

