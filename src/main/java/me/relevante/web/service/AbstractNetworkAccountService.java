package me.relevante.web.service;

import me.relevante.api.NetworkBasicAuthCredentials;
import me.relevante.api.NetworkCredentials;
import me.relevante.api.NetworkOAuthCredentials;
import me.relevante.api.OAuthTokenPair;
import me.relevante.model.Indexable;
import me.relevante.model.NetworkFullUser;
import me.relevante.model.NetworkProfile;
import me.relevante.model.RelevanteAccount;
import me.relevante.model.RelevanteContext;
import me.relevante.network.Network;
import me.relevante.web.model.RegisterResult;
import me.relevante.web.model.oauth.NetworkOAuthManager;
import me.relevante.web.persistence.RelevanteAccountRepo;
import me.relevante.web.persistence.RelevanteContextRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.UUID;

@Component
public abstract class AbstractNetworkAccountService<N extends Network, P extends NetworkProfile<N, ?>, F extends NetworkFullUser<N, ?>> implements NetworkAccountService<N, F> {

    private static final long MINUTE_IN_SECONDS = 60;
    private static final long HOUR_IN_SECONDS = 60 * MINUTE_IN_SECONDS;
    private static final long DAY_IN_SECONDS = 24 * HOUR_IN_SECONDS;
    private static final long MIN_TIME_BETWEEN_ACTIONS_IN_SECONDS = 30;
    private static final long MAX_TIME_WITHOUT_MONITORING_IN_SECONDS = 2 * DAY_IN_SECONDS;
    private static final long MAX_MONITORED_USERS = 5000;
    private static final String DEFAULT_ACTIONS_TIME_RANGE = "08:00-22:00";

    protected RelevanteAccountRepo relevanteAccountRepo;
    protected RelevanteContextRepo relevanteContextRepo;
    protected CrudRepository<F, String> fullUserRepo;
    protected QueueService queueService;
    protected N network;

    @Autowired
    public AbstractNetworkAccountService(RelevanteAccountRepo relevanteAccountRepo,
                                         RelevanteContextRepo relevanteContextRepo,
                                         CrudRepository<F, String> fullUserRepo,
                                         QueueService queueService,
                                         N network) {
        this.relevanteAccountRepo = relevanteAccountRepo;
        this.relevanteContextRepo = relevanteContextRepo;
        this.fullUserRepo = fullUserRepo;
        this.queueService = queueService;
        this.network = network;
    }

    @Override
    public F getNetworkUser(RelevanteAccount relevanteAccount) {
        if (!relevanteAccount.isNetworkConnected(network)) {
            return null;
        }
        NetworkCredentials credentials = relevanteAccount.getCredentials(network);
        F fullUser = fullUserRepo.findOne(credentials.getUserId());
        return fullUser;
    }

    @Override
    public RegisterResult<F> registerOAuthNetworkAccount(String existingRelevanteId,
                                                         String oAuthRequestToken,
                                                         String oAuthRequestSecret,
                                                         String oAuthRequestOriginalToken,
                                                         String oAuthRequestVerifier) {
        if (!oAuthRequestOriginalToken.equals(oAuthRequestToken)) {
            throw new IllegalArgumentException("Callback token is wrong");
        }
        NetworkOAuthManager<N> oAuthManager = createNetworkOAuthManager();
        OAuthTokenPair<N> accessTokenPair = oAuthManager.obtainAccessToken(oAuthRequestToken, oAuthRequestSecret, oAuthRequestVerifier);
        if (accessTokenPair == null) {
            throw new IllegalArgumentException("Couldn't get " + network.getName() + " oauth access token");
        }

        P profile = createProfileFromOAuthData(accessTokenPair);
        RelevanteAccount relevanteAccount = (existingRelevanteId != null) ? relevanteAccountRepo.findOne(existingRelevanteId) : relevanteAccountRepo.findOneByCredentialsNetworkAndCredentialsUserId(network.getName(), profile.getId());

        F fullUser = fullUserRepo.findOne(profile.getId());
        if (fullUser == null) {
            fullUser = createFullUserFromProfile(profile);
            fullUserRepo.save(fullUser);
        }
        if (getNetwork() instanceof Indexable) {
            queueService.sendUserIndexMessage(network, fullUser.getId());
        }

        boolean isNewUser = false;
        if (relevanteAccount == null) {
            relevanteAccount = createNewRelevanteAccount();
            isNewUser = true;
        }
        NetworkCredentials<N> credentials = new NetworkOAuthCredentials<>(network, fullUser.getId(), oAuthManager.getOAuthConsumerKeyPair(), accessTokenPair);
        populateRelevanteAccountWithNetworkData(relevanteAccount, fullUser);
        relevanteAccount.putCredentials(credentials);
        relevanteAccountRepo.save(relevanteAccount);

        return new RegisterResult<>(relevanteAccount, fullUser, isNewUser);
    }

    @Override
    public RegisterResult<F> registerBasicAuthNetworkAccount(String existingRelevanteId,
                                                             String url,
                                                             String username,
                                                             String password) {
        P profile = createProfileFromBasicAuthData(url, username, password);
        RelevanteAccount relevanteAccount = (existingRelevanteId != null) ? relevanteAccountRepo.findOne(existingRelevanteId) : relevanteAccountRepo.findOneByCredentialsNetworkAndCredentialsUserId(network.getName(), profile.getId());

        F fullUser = fullUserRepo.findOne(profile.getId());
        if (fullUser == null) {
            fullUser = createFullUserFromProfile(profile);
            fullUserRepo.save(fullUser);
        }
        if (getNetwork() instanceof Indexable) {
            queueService.sendUserIndexMessage(network, fullUser.getId());
        }

        boolean isNewUser = false;
        if (relevanteAccount == null) {
            relevanteAccount = createNewRelevanteAccount();
            isNewUser = true;
        }
        NetworkCredentials<N> credentials = new NetworkBasicAuthCredentials<>(network, fullUser.getId(), url, username, password);
        populateRelevanteAccountWithNetworkData(relevanteAccount, fullUser);
        relevanteAccount.putCredentials(credentials);
        relevanteAccountRepo.save(relevanteAccount);

        return new RegisterResult<>(relevanteAccount, fullUser, isNewUser);
    }

    public void unregisterNetworkAccount(String relevanteId) {
        Network network = getNetwork();
        RelevanteAccount relevanteAccount = relevanteAccountRepo.findOne(relevanteId);
        relevanteAccount.removeCredentials(network);
        relevanteAccountRepo.save(relevanteAccount);
    }

    protected abstract NetworkOAuthManager<N> createNetworkOAuthManager();

    protected abstract P createProfileFromOAuthData(OAuthTokenPair<N> oAuthTokenPair);

    protected abstract P createProfileFromBasicAuthData(String url, String username, String password);

    protected abstract F createFullUserFromProfile(P profile);

    protected abstract void populateRelevanteAccountWithNetworkData(RelevanteAccount relevanteAccount, F fullUser);

    private RelevanteAccount createNewRelevanteAccount() {

        RelevanteAccount relevanteAccount = new RelevanteAccount();
        relevanteAccount.setId(UUID.randomUUID().toString());
        relevanteAccount.setCreationTimestamp(new Date());
        relevanteAccount.setLastAccessTimestamp(new Date());
        relevanteAccount.setMinTimeBetweenActionsInSeconds(MIN_TIME_BETWEEN_ACTIONS_IN_SECONDS);
        relevanteAccount.setMaxTimeWithoutMonitoringInSeconds(MAX_TIME_WITHOUT_MONITORING_IN_SECONDS);
        relevanteAccount.setMaxMonitoredUsers(MAX_MONITORED_USERS);
        relevanteAccount.getActionsTimeRanges().add(DEFAULT_ACTIONS_TIME_RANGE);

        RelevanteContext relevanteContext = new RelevanteContext(relevanteAccount.getId());

        relevanteContextRepo.save(relevanteContext);
        relevanteAccountRepo.save(relevanteAccount);

        return relevanteAccount;
    }

}
